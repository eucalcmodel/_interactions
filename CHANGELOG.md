# Changelog

## Unreleased
### Added
- Item 1
- Item 2
### Changed
- Item 1
- Item 2
### Removed
- Item 1
- Item 2

## 1.4
### Added
- Post-0.9 adds
### Changes
- Add check of values (Countries, Years)
- Add check of redundancy
- Add check of Column types
- Add metanodes : 'Multi CHECK' and 'Country CHECK'
### Removes
### Fixed 
- Group By in Lifestyle Module (local change)

## 0.9
- Initial version, integrating all modules